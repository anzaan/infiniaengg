function changeActivePillsClass(elem){
  var tablist = document.getElementsByClassName('nav navbar-nav');
  var tabs = tablist[0].getElementsByTagName('li');
  for(var i =0; i<tabs.length; i++){
    tabs[i].classList.remove('active');
  }
  elem.classList.add('active');
}

function changeBodyContent(page){
  // console.log(page);
  $('#content').load(page);
}